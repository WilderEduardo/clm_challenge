const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const movieSchema = new Schema({
    Title: String,
    Year: Number,
    Released: String,
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    Ratings: [
        {
            Source: String,
            Value: String
        }
    ],
    Poster: String
});

const Movies = mongoose.model('collection1', movieSchema);

module.exports = Movies;