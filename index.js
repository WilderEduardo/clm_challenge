//Mongo Databse
const mongoose = require('mongoose');
const db = mongoose.connection;
const host = 'mongodb+srv://Wilder:p7joPTVneHzXOu8L@cluster0.vfhaz.mongodb.net/moviesdatabase?retryWrites=true&w=majority';
const dbupdate = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}
mongoose.connect(host, dbupdate);

db.on('error', (err) => console.log('Error en la conexión de la Base de Datos'));
db.on('connected', ()=> console.log('Servidor conectado a la Base de Datos'))

//Paquetes
const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
require('dotenv').config();
const Movies = require('./models/movies');
const static = require('koa-static');
const parser = require('koa-bodyparser');
const koaNunjucks = require('koa-nunjucks-2');
/* const cors = require('@koa/cors'); */



router.get('/allmovies', async ctx => {
    return await Movies.find({}, (err, results)=>{
        ctx.render('allmovies', {
            movies: results
        })
    })
})

router.get('/', async ctx => {
    return await ctx.render('index')
})

router.get('/:movie', async ctx=>{
    return await Movies.find({Title: ctx.params.movie}, (err, results)=>{
        ctx.render('movie', {
            movie: results
        });
    });
});


router.get('/api/movies', async ctx=>{
    return await Movies.find({}, (err, results)=>{
        ctx.body = {results};
        ctx.status = 200;
    });
});

router.post('/api/movies',  async ctx => {
    ctx.status = 200;
    ctx.body = 'Pelicula Agregada a la Base de Datos';
    Movies.create(ctx.request.body, (err, result)=>{
        console.log(result);
    })
});


//Middlewares
app.use(parser());
app.use(koaNunjucks({
    ext: 'html',
    path: './views',
    nunjucksConfig: {
      trimBlocks: true
    }
  }));
app.use(router.routes()).use(router.allowedMethods());
app.use(static('./public'));


app.listen(3000, ()=> console.log('Servidor corriendo en puerto 3000'))